import React from 'react';
import Theme from './../Theme'
import {
    Appear,
    Deck,
    Heading,
    Image,
    ListItem,
    List,
    Slide,
    Text,
} from 'spectacle';

const App = () => (
    <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={Theme}
        progress='bar'
    >
        <Slide transition={['zoom']} bgColor="secondary" progressColor="primary">
            <Image style={{marginBottom: '60px'}} src="https://jigsaw.tighten.co/assets/img/jigsaw-logo.svg" height={200} />
            <Heading size={3} lineHeight={1} textColor="primary">
                Laravel Mix & Blade
            </Heading>
            <Text margin="10px 0 0" textColor="primary" size={5} bold>
                Utilizando o Jigsaw
            </Text>
        </Slide>
        <Slide transition={['fade']} bgColor="quaternary" progressColor="tertiary" >
            <Text fit textColor="primary" size={5} bold>
                 O que é o Jigsaw?
            </Text>
        </Slide>
        <Slide transition={['spin']} bgColor="primary" textColor="tertiary" progressColor="secondary">
            <Text fit textColor="secondary" size={5} bold>
                Por que o Jigsaw?
            </Text>
            <Text textColor="quaternary" size={1} bold>
                Outra alternativa... Jekyll!
            </Text>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary" textColor="primary" progressColor="secondary">
            <Text fit textColor="quaternary" size={5} bold>
                O que precisamos conhecer?
            </Text>
            <List bulletStyle="cross">
                <Appear>
                    <ListItem>HTML & CSS - Básico</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Javascript - Básico</ListItem>
                </Appear>
                <Appear>
                    <ListItem>php - Básico</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Comandos Linux - Básico</ListItem>
                </Appear>
            </List>
        </Slide>
        <Slide transition={['fade']} bgColor="quaternary" progressColor="primary">
            <Text fit textColor="tertiary" size={5} bold>
                O que vamos ver?
            </Text>
            <List bulletStyle="cross">
                <Appear>
                    <ListItem>Composer</ListItem>
                </Appear>
                <Appear>
                    <ListItem>NPM</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Laravel Mix</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Sass</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Blade</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Bootstrap4 e Templates</ListItem>
                </Appear>
                <Appear>
                    <ListItem>Gilab CI</ListItem>
                </Appear>
            </List>
        </Slide>
        <Slide transition={['fade']} bgColor="primary" textColor="tertiary" progressColor="secondary">
            <Text fit textColor="secondary" size={5} bold>
                O que queremos fazer?
            </Text>
            <Text textColor="quaternary" size={1} bold>
                Sites Estáticos Escaláveis em x minutos!
            </Text>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary" progressColor="primary">
            <Text fit textColor="primary" size={5} bold>
                Vamos começar!
            </Text>
        </Slide>
        <Slide transition={['slide']} bgColor="quaternary" textColor="tertiary" progressColor="secondary">
            <Text fit textColor="primary" size={5} bold>
                O que é o Composer?
            </Text>
        </Slide>
        <Slide transition={['scale']} bgColor="tertiary" progressColor="secondary">
            <Text fit textColor="secondary" size={5} bold>
                Instalando o Jigsaw
            </Text>
        </Slide>
        <Slide transition={['slide']} bgColor="quaternary" textColor="tertiary" progressColor="secondary">
            <Text fit textColor="primary" size={5} bold>
                O que é NPM?
            </Text>
        </Slide>
        <Slide transition={['scale']} bgColor="tertiary" progressColor="secondary">
            <Text fit textColor="secondary" size={5} bold>
                Instalando o Laravel Mix
            </Text>
        </Slide>
    </Deck>
)

export default App
