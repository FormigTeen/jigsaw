import TerminalStyle from "spectacle-terminal"
import React from 'react';

const Terminal = ({children}) => <TerminalStyle title="1. formigteen@localhost: ~(bash)" output={children} />

export default Terminal

