import createTheme from 'spectacle/lib/themes/default';

const Theme = createTheme(
    {
        primary: '#1F2022',
        secondary: '#2ECC71',
        tertiary: '#E74C3C',
        quaternary: '#ECF0F1',
    },
    {
        primary: 'Montserrat',
        secondary: 'Helvetica',
    }
);

export default Theme
